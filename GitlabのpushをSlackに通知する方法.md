# 1. GitlabのpushをSlackに通知する方法
<!-- TOC -->

- [1. GitlabのpushをSlackに通知する方法](#1-gitlabのpushをslackに通知する方法)
    - [1.1. SlackのIncoming Webhookから通知用のURLを取得](#11-slackのincoming-webhookから通知用のurlを取得)
    - [1.2. Gitlabの設定](#12-gitlabの設定)
    - [1.3. 試しにpush](#13-試しにpush)

<!-- /TOC -->
## 1.1. SlackのIncoming Webhookから通知用のURLを取得
Slackの管理画面から他アプリケーションで通知をするためのエンドポイントを取得する。  
[SlackのIncoming-Webhok設定画面](https://my.slack.com/services/new/incoming-webhook)  
![設定画面１](./img/3.JPG) 
![設定画面２](./img/4.JPG)  

## 1.2. Gitlabの設定
プロジェクト内にあるintegrationsを選択  
![設定画面１](./img/1.JPG)  
画面下”Project services”の中から「Slack notifications」を選択  
![設定画面２](./img/2.JPG)  
Slackから取得したエンドポイントをWebhookに設定  
![設定画面３](./img/5.JPG)  

## 1.3. 試しにpush
URL付きで通知が出るのでさっと確認出来て便利です。  
![設定画面１](./img/6.JPG)  
